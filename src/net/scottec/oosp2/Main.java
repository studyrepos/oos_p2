package net.scottec.oosp2;

public class Main {

    public static void main(String[] args) {
        System.out.println("OSS - Praktikum 2");

        System.out.print("Starte Benutzerverwaltung... ");
        BenutzerVerwaltungAdmin admin = new BenutzerVerwaltungAdmin();
        System.out.println("OK");

        Benutzer a, b, c;

        a = new Benutzer("Nutzer1", "abc".toCharArray());
        b = new Benutzer("Nutzer2", "123".toCharArray());
        c = new Benutzer("Nutzer1", "abc".toCharArray());

        System.out.println("Tests auf Gleichheit (equals):");
        System.out.println("Benutzer A equals B: (False) -> " + a.equals(b));
        System.out.println("Benutzer A equals C: (True) -> " + a.equals(c));
        System.out.println("Benutzer B equals C: (False) -> " + b.equals(c));

        System.out.println("\nTest auf Funktion von BenutzerverwaltungAdmin:");
        System.out.println("\nTest Benutzer eintragen:");
        try {
            a = new Benutzer("NameOne", "PassOne".toCharArray());
            System.out.print("Benutzer eintragen: " + b.toString() + " (OK, unique) ->");
            admin.benutzerEintragen(a);
            System.out.println("OK");
        }
        catch (BenutzerDoppeltExeption exp) {
            System.out.println(exp.getMessage());
        }
        System.out.println("Benutzer (" + b.toString() + " okay? -> " + admin.benutzerOk(b) + "\n");

        try {
            b = new Benutzer("NameTwo", "PassTwo".toCharArray());
            System.out.print("Benutzer eintragen: " + b.toString() + " (OK, unique) ->");
            admin.benutzerEintragen(b);
            System.out.println("OK");
        }
        catch (BenutzerDoppeltExeption exp) {
            System.out.println(exp.getMessage());
        }
        System.out.println("Benutzer (" + b.toString() + " okay? -> " + admin.benutzerOk(b) + "\n");

        try {
            b = new Benutzer("NameThree", "PassThree".toCharArray());
            System.out.print("Benutzer eintragen: " + b.toString() + " (OK, unique) ->");
            admin.benutzerEintragen(b);
            System.out.println("OK");
        }
        catch (BenutzerDoppeltExeption exp) {
            System.out.println(exp.getMessage());
        }
        System.out.println("Benutzer (" + b.toString() + " okay? -> " + admin.benutzerOk(b) + "\n");

        try {
            b = new Benutzer("NameOne", "PassOne".toCharArray());
            System.out.print("Benutzer eintragen: " + b.toString() + " (Fehler, doppelt) ->");
            admin.benutzerEintragen(b);
            System.out.println("OK");
        }
        catch (BenutzerDoppeltExeption exp) {
            System.out.println(exp.getMessage());
        }
        System.out.println("Benutzer (" + b.toString() + " okay? -> " + admin.benutzerOk(b) + "\n");

        try {
            b = new Benutzer("NameTwo", "PassTwoTwo".toCharArray());
            System.out.print("Benutzer eintragen: " + b.toString() + " (Ok, Name doppelt, Passwort unique) ->");
            admin.benutzerEintragen(b);
            System.out.println("OK");
        }
        catch (BenutzerDoppeltExeption exp) {
            System.out.println(exp.getMessage());
        }
        System.out.println("Benutzer (" + b.toString() + " okay? -> " + admin.benutzerOk(b) + "\n");

        try {
            b = new Benutzer("NameFour", "PassThree".toCharArray());
            System.out.print("Benutzer eintragen: " + b.toString() + " (Ok, Name unique, Passwort doppelt) ->");
            admin.benutzerEintragen(b);
            System.out.println("OK");
        }
        catch (BenutzerDoppeltExeption exp) {
            System.out.println(exp.getMessage());
        }
        System.out.println("Benutzer (" + b.toString() + " okay? -> " + admin.benutzerOk(b) + "\n");

        System.out.println("Benutzer erzeugen, aber nicht eintragen:");
        b = new Benutzer("NameFive", "PassFive".toCharArray());
        System.out.println("Benutzer (" + b.toString() + ") okay? -> " + admin.benutzerOk(b) + "\n");

        System.out.println("Teste Benutzer löschen:");

        try {
            System.out.print("Benutzer löschen: " + b.toString() + " (Fehler) -> ");
            admin.BenutzerLoeschen(b);
            System.out.println("OK");
        }
        catch (BenutzerNichtGefundenException exp) {
            System.out.println(exp.getMessage());
        }

        try {
            System.out.print("Benutzer löschen: " + a.toString() + " (OK) -> ");
            admin.BenutzerLoeschen(a);
            System.out.println("OK");
        }
        catch (BenutzerNichtGefundenException exp) {
            System.out.println(exp.getMessage());
        }
    }
}
