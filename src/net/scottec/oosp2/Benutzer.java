package net.scottec.oosp2;

import java.util.Arrays;

/**
 * Eine einfache Benutzerklasse
 * Benutzer haben lediglich UserId und Passwort
 */
public class Benutzer {

    public Benutzer() {
        this.userId = "";
        this.passWort = new char[0];
    }

    /**
     * Konstruktor
     * @param _userId   : Eindeutige ID des Benutzers
     * @param _passWort : Passwort des Benutzers
     */
    public Benutzer(String _userId, char[] _passWort) {
        this.userId = _userId;
        this.passWort = _passWort.clone();
    }

    /**
     * Prüft auf Attributgleicheit, sofern Klasse stimmt.
     * Überlagert(Override) Object.equals
     * @param _benutzer : Zu vergleichender Benutzer
     * @return  True    : Attribute identisch
     *          False   : Attribute unterscheiden sich
     */
    public boolean equals(Object _benutzer) {
        return (_benutzer instanceof Benutzer
        && this.userId.equals(((Benutzer) _benutzer).userId)
        && String.copyValueOf(this.passWort).equals(
                String.copyValueOf(((Benutzer) _benutzer).passWort)));
    }

    /**
     * Eine einfache Ausgabe
     * @return  String  : Eine einfache Stringdarstellung des Benutzers
     */
    public String toString() {
        return ("Benutzer: userId: '" + this.userId
                + "' - passWort: " + Arrays.toString(this.passWort));
    }

    // Private Attribute
    private String userId;
    private char[] passWort;

}
