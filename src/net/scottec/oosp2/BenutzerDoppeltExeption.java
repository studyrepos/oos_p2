package net.scottec.oosp2;

/**
 * Exception
 * Kennzeichnet Dopplung in Datenhaltung
 */
public class BenutzerDoppeltExeption extends Exception {
    public BenutzerDoppeltExeption(String message) {
        super(message);
    }
}
