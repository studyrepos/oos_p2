package net.scottec.oosp2;

import java.util.ArrayList;
import java.util.List;

/**
 * Eine sehr einfache, nicht persistende Datenverwaltung.
 * Speichert Benutzer in ArrayList<>.
 */
public class BenutzerVerwaltungAdmin implements BenutzerVerwaltung {

    public BenutzerVerwaltungAdmin(){}

    /**
     * Versucht einen Benutzer in das System aufzunehmen.
     * Existiert der Benutzer bereits, wird eine Exception geworfen.
     * Ist die Aufnahme erfolgreich, terminiert die Funktion ohne Meldung.
     * @param _benutzer : Der aufzunehmende Benutzer
     * @throws BenutzerDoppeltExeption  : Exception, falls Benutzer bereits vorhanden
     */
    public void benutzerEintragen(Benutzer _benutzer) throws BenutzerDoppeltExeption{
        if (!this.benutzerList.contains(_benutzer)) {
            this.benutzerList.add(_benutzer);
        }
        else {
            throw new BenutzerDoppeltExeption("Benutzer bereits vorhanden");
        }
    }

    /**
     * Prüft, ob Benutzer in Datenhaltung vorhanden
     * @param _benutzer : Zu überprüfender Benutzer
     * @return  True    : Benutzer gefunden
     *          False   : Benutzer nicht gefunden
     */
    public boolean benutzerOk(Benutzer _benutzer) {
        return this.benutzerList.contains(_benutzer);
    }

    /**
     * Versucht einen Benutzer aus der Datenhaltung zu löschen
     * Terminiert still, falls Löschen erfolgreich, andernfalls wird
     * eine Exception geworfen
     * @param _benutzer : Zu löschender Benutzer
     * @throws BenutzerNichtGefundenException   : Benutzer nicht Gefunden
     */
    public void BenutzerLoeschen(Benutzer _benutzer) throws BenutzerNichtGefundenException {
        if (!this.benutzerList.remove(_benutzer)){
            throw new BenutzerNichtGefundenException("Benutzer nicht gefunden");
        }
    }

    // private Attribute
    private List<Benutzer> benutzerList = new ArrayList<>();
}
